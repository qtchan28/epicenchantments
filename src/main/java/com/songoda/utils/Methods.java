package com.songoda.utils;

import org.bukkit.ChatColor;

import java.util.List;
import java.util.stream.Collectors;

public class Methods {
    public static String convertToInvisibleString(String s) {
        StringBuilder hidden = new StringBuilder();
        for (char c : s.toCharArray()) hidden.append(ChatColor.COLOR_CHAR + "").append(c);
        return hidden.toString();
    }
    public static String format(String form) {
        return ChatColor.translateAlternateColorCodes('&', form);
    }
    public static List<String> formatList(List<String> form) {
        return form.stream().map(Methods::format).collect(Collectors.toList());
    }
}
