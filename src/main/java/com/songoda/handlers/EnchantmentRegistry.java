package com.songoda.handlers;

import com.songoda.EpicEnchants;
import com.songoda.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class EnchantmentRegistry {
    private Map<String, Enchantment> registeredEnchantments = new HashMap<>();

    public Enchantment registerEnchantment(String name, Enchantment enchantment) {
        registeredEnchantments.put(name.toLowerCase(), enchantment);
        return enchantment;
    }

    public Enchantment unregisterEnchantment(Enchantment enchantment) {
        registeredEnchantments.remove(enchantment);
        return enchantment;
    }

    public Enchantment getRegisteredEnchantment(String name) {
        return registeredEnchantments.get(name.toLowerCase());
    }

    public boolean isRegisteredEnchantment(String enchantment) {
        return registeredEnchantments.containsKey(enchantment.toLowerCase());
    }

    public Map<String, Enchantment> getRegisteredEnchantments() {
        return Collections.unmodifiableMap(registeredEnchantments);
    }

    public void clear() {
        registeredEnchantments.clear();
    }

    public boolean hasEnchantment(ItemStack itemStack, Enchantment enchantment) {
        return EpicEnchants.getInstance().getItemEnchantmentHandler().hasEnchantment(itemStack, enchantment);
    }
}
