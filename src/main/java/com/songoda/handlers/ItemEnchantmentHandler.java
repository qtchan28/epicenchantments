package com.songoda.handlers;

import com.songoda.EpicEnchants;
import com.songoda.enchantments.Enchantment;
import com.songoda.utils.Methods;
import com.songoda.utils.RomanNumber;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemEnchantmentHandler {
    private final EpicEnchants instance;
    public ItemEnchantmentHandler(EpicEnchants instance) { this.instance = instance; }

    public String getLevel(ItemStack itemStack, Enchantment enchantment) {
        if (!itemStack.hasItemMeta() || !itemStack.getItemMeta().hasLore()) return "";

        List<String> lore = itemStack.getItemMeta().getLore();
        for (String str : lore) {
            if (str.startsWith(Methods.format(enchantment.getColor() + enchantment.getName()))) {
                return str.split(" ")[str.split(" ").length - 1];
            }
        }
        return "";
    }
    public boolean hasEnchantment(ItemStack itemStack, Enchantment enchantment) {
        if (itemStack == null || !itemStack.hasItemMeta() || !itemStack.getItemMeta().hasLore()) return false;
        List<String> lore = itemStack.getItemMeta().getLore();
        for (String str : lore) {
            if (str.contains(Methods.format(enchantment.getName()))) return true;
        }
        return false;
    }
    public ItemStack addEnchantment(ItemStack itemStack, Enchantment enchantment, int level) {

        ItemMeta itemMeta = itemStack.getItemMeta();
        List<String> lore;

        if (hasEnchantment(itemStack, enchantment))
            return null;
        else if (itemMeta.hasLore()) {
            lore = itemMeta.getLore();
        } else {
            lore = new ArrayList<>();
        }

        String levelIdentifier = RomanNumber.toRoman(level);

        if (level >= 10) levelIdentifier = Integer.toString(level);

        lore.add(Methods.convertToInvisibleString(enchantment.getName() + ":" + level + ":") +
                ChatColor.translateAlternateColorCodes('&',"&7" +enchantment.getName() + " " + levelIdentifier));

        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);


        return itemStack;
    }

    public Enchantment valueOf(String name) {
        for (Enchantment enchant : EpicEnchants.getInstance().getEnchantmentRegistry().getRegisteredEnchantments().values()) {
            if (enchant.getName().equalsIgnoreCase(Methods.format(name))) {
               return enchant;
            }
        }

        return null;
    }

    public Enchantment valueOf(ItemStack is) {
        if (is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore()) {
            for (Enchantment enchant : EpicEnchants.getInstance().getEnchantmentRegistry().getRegisteredEnchantments().values()) {
                if (is.getItemMeta().getDisplayName().startsWith(Methods.format(enchant.getName()))) {
                    return enchant;
                }
            }
        }
        return null;
    }
}
