package com.songoda.handlers;

import com.songoda.EpicEnchants;
import com.songoda.api.ItemType;
import com.songoda.enchantments.Enchantment;
import com.songoda.utils.Methods;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class CommandHandler implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0 &&
                command.getName().equalsIgnoreCase("epicenchants") ||
                command.getName().equalsIgnoreCase("ee")) {
            sender.sendMessage(Methods.format("&7EpicEnchants by &5Songoda"));
            sender.sendMessage(Methods.format("&a============================="));
            sender.sendMessage(Methods.format("&eHelp &7Menu"));
            sender.sendMessage(Methods.format(""));
            sender.sendMessage(Methods.format("&7/ee enchants: List all the enchantments in this plugin."));
            sender.sendMessage(Methods.format("&7/ee creative: Get all the items from the plugin"));
            sender.sendMessage(Methods.format("&7/ee help: Brings you to this page."));
            sender.sendMessage(Methods.format("&a============================="));
        } else if (args.length >= 1) {
            if (args[0].equalsIgnoreCase("help")) {
                sender.sendMessage(Methods.format("&7EpicEnchants by &5Songoda"));
                sender.sendMessage(Methods.format("&a============================="));
                sender.sendMessage(Methods.format("&eHelp &7Menu"));
                sender.sendMessage(Methods.format(""));
                sender.sendMessage(Methods.format("&7/ee enchants: List all the enchantments in this plugin."));
                sender.sendMessage(Methods.format("&7/ee creative: Get all the items from the plugin"));
                sender.sendMessage(Methods.format("&7/ee help: Brings you to this page."));
                sender.sendMessage(Methods.format("&a============================="));
            } else if (args[0].equalsIgnoreCase("creative") && sender.hasPermission("epicenchants.creative")) {
                if (!(sender instanceof Player)) sender.sendMessage("You must be a player!");

                Inventory inventory = Bukkit.createInventory(null, 9, Methods.format("EpicEnchants Creative Menu"));
                ItemStack book = new ItemStack(Material.BOOK);
                ItemMeta bookMeta = book.getItemMeta();
                bookMeta.setDisplayName(Methods.format("&d&lEnchantment Books"));
                bookMeta.setLore(Methods.formatList(Arrays.asList("&9Click me to display all of the", "&5custom enchantment books&9!")));
                book.setItemMeta(bookMeta);
                inventory.setItem(0, book);

                ItemStack dust = new ItemStack(Material.FIREBALL);
                ItemMeta dm = dust.getItemMeta();
                dm.setDisplayName(Methods.format("&c&lDust&c Menu"));
                dm.setLore(Methods.formatList(Arrays.asList("&c&lClick me&c to go to the Dust Menu!")));
                dust.setItemMeta(dm);

                inventory.setItem(1, dust);
                Player player = (Player) sender;
                player.openInventory(inventory);
            }
            if (args.length == 1 && args[0].equalsIgnoreCase("give")) sender.sendMessage(Methods.format("&cWrong usage!"));
            for (Enchantment enchant : EpicEnchants.getInstance().getEnchantmentRegistry().getRegisteredEnchantments().values()) {
                if (args[0].equalsIgnoreCase("give") && args.length == 2 && args[1].equalsIgnoreCase(enchant.getName())) {
                    if (!(sender instanceof Player)) return false;

                    Player player = (Player) sender;
                    player.sendMessage(Methods.format("&8(&7EpicEnchants&8)&7 Giving enchant " + enchant.getColor() + args[1]));
                    player.getInventory().addItem(enchant.apply(new ItemStack(Material.DIAMOND_HELMET)));

                }
            }
        }
        return true;
    }
}
