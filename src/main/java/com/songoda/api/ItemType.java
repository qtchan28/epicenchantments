package com.songoda.api;

import org.bukkit.Material;

import java.util.Arrays;
import java.util.List;

public enum ItemType {
    PICKAXE("Pickaxe", Material.DIAMOND_PICKAXE, Material.GOLD_PICKAXE, Material.IRON_PICKAXE, Material.STONE_PICKAXE,Material.WOOD_PICKAXE),
    SWORD("Sword", Material.DIAMOND_SWORD, Material.GOLD_SWORD, Material.IRON_SWORD, Material.STONE_SWORD, Material.WOOD_SWORD),
    HOE("Hoe", Material.DIAMOND_HOE, Material.GOLD_HOE, Material.IRON_HOE, Material.STONE_HOE, Material.WOOD_HOE),
    SPADE("Spade", Material.DIAMOND_SPADE, Material.GOLD_SPADE, Material.IRON_SPADE, Material.STONE_SPADE, Material.WOOD_SPADE),
    AXE("Axe", Material.DIAMOND_AXE, Material.GOLD_AXE, Material.IRON_AXE, Material.STONE_AXE, Material.WOOD_AXE),
    BOW("Bow", Material.BOW),
    HELMET("Helmet", Material.DIAMOND_HELMET, Material.LEATHER_HELMET, Material.CHAINMAIL_HELMET, Material.GOLD_HELMET, Material.IRON_HELMET),
    CHESTPLATE("Chestplate", Material.DIAMOND_CHESTPLATE, Material.LEATHER_CHESTPLATE, Material.CHAINMAIL_CHESTPLATE, Material.GOLD_CHESTPLATE, Material.IRON_CHESTPLATE),
    LEGGINGS("Leggings", Material.DIAMOND_LEGGINGS, Material.LEATHER_LEGGINGS, Material.CHAINMAIL_LEGGINGS, Material.GOLD_LEGGINGS, Material.IRON_LEGGINGS),
    BOOTS("Boots", Material.DIAMOND_BOOTS, Material.LEATHER_BOOTS, Material.CHAINMAIL_BOOTS, Material.GOLD_BOOTS, Material.IRON_BOOTS);
    private String name;
    private Material[] materials;
    private ItemType(String name, Material... materials) {
        this.name = name;
        this.materials = materials;
    }
    public List<Material> getMaterials() {
        return Arrays.asList(materials);
    }
}
