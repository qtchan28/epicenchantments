package com.songoda.enchantments.tool;

import com.songoda.EpicEnchants;
import com.songoda.api.ItemType;
import com.songoda.enchantments.Enchantment;
import com.songoda.handlers.ItemEnchantmentHandler;
import com.songoda.utils.Methods;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;

public class DrunkenEnchantment extends Enchantment {

    public DrunkenEnchantment() {
        super("Drunken", "III", Methods.formatList(Arrays.asList("&7You are slowed and have mining fatigue", "&7But have &cstrength&7 as well.")), new ItemType[] { ItemType.HELMET }, "&6");
    }

    @EventHandler
    public void onEquip(InventoryClickEvent event) {
        if (!event.getSlotType().equals(InventoryType.SlotType.ARMOR) || !EpicEnchants.getInstance().getItemEnchantmentHandler().hasEnchantment(event.getCursor(), this)) return;
        Player player = (Player) event.getWhoClicked();
        PotionEffect strength = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 10000000, 1);
        PotionEffect slowness = new PotionEffect(PotionEffectType.SLOW, 10000000, 1);
        PotionEffect fatigue = new PotionEffect(PotionEffectType.SLOW_DIGGING,10000000, 1);
        player.addPotionEffect(strength);
        player.addPotionEffect(slowness);
        player.addPotionEffect(fatigue);
     }
}
