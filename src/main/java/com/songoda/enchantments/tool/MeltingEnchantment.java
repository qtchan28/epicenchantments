package com.songoda.enchantments.tool;

import com.songoda.EpicEnchants;
import com.songoda.api.ItemType;
import com.songoda.enchantments.Enchantment;
import com.songoda.utils.Methods;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class MeltingEnchantment extends Enchantment {
    public MeltingEnchantment() {
        super("Melting", "III", Methods.formatList(Arrays.asList("&7When mining there is a chance to", "&6automatically smelt &7ores.")), new ItemType[]{ItemType.PICKAXE}, "&6");
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (event.getPlayer().getInventory().getItemInMainHand() == null || !EpicEnchants.getInstance().getEnchantmentRegistry().hasEnchantment(player.getInventory().getItemInMainHand(), this))
            return;

        ItemStack stack = player.getInventory().getItemInMainHand();
        int amount = 1;
        if (EpicEnchants.getInstance().getItemEnchantmentHandler().getLevel(stack, this).equalsIgnoreCase("I")) amount = 2;
        if (EpicEnchants.getInstance().getItemEnchantmentHandler().getLevel(stack, this).equalsIgnoreCase("II")) amount = 3;
        if (EpicEnchants.getInstance().getItemEnchantmentHandler().getLevel(stack, this).equalsIgnoreCase("III")) amount = 4;
        if (event.getBlock().getType() == Material.IRON_ORE) {
            event.getBlock().setType(Material.AIR);
            event.getPlayer().sendMessage(Methods.format("&3Ore &6&lSmelted"));
            event.getBlock().getWorld().dropItem(event.getBlock().getLocation().add(0.5,0.5,0.5), new ItemStack(Material.IRON_INGOT, amount));
        } else if (event.getBlock().getType() == Material.GOLD_ORE) {
            event.getBlock().setType(Material.AIR);
            event.getPlayer().sendMessage(Methods.format("&aOre &6&lSmelted"));
            event.getBlock().getWorld().dropItem(event.getBlock().getLocation().add(0.5,0.5,0.5), new ItemStack(Material.GOLD_INGOT, amount));
        }
    }
}