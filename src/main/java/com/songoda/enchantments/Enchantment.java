package com.songoda.enchantments;

import com.songoda.EpicEnchants;
import com.songoda.api.ItemType;
import com.songoda.utils.Methods;
import com.songoda.utils.RomanNumber;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Destructuctor
 * Date: 11/24/18
 */
public abstract class Enchantment implements Listener {
    private String name;
    private String maxLevel;
    private List<String> description;
    private ItemType[] itemTypes;
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Enchantment(String name, String maxLevel, List<String> description, ItemType[] itemTypes, String color) {
        this.name = name;
        this.maxLevel = maxLevel;
        this.description = description;
        this.itemTypes = itemTypes;
        this.color = color;
        EpicEnchants.getInstance().getEnchantmentRegistry().registerEnchantment(name, this);
        Bukkit.getPluginManager().registerEvents(this, EpicEnchants.getInstance());
    }

    /**
     * Gets the name of the enchantment.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the enchantment
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(String maxLevel) {
        this.maxLevel = maxLevel;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public ItemType[] getItemTypes() {
        return itemTypes;
    }

    public void setItemTypes(ItemType[] itemTypes) {
        this.itemTypes = itemTypes;
    }

    public void onHitEntity(Entity entity) {
    }

    public void onHitPlayer(Player player) {
    }


    public void onUse(Player player) {
    }

    public ItemStack toBook(Material mat) {
        ItemStack stack = new ItemStack(mat);
        ItemMeta sm = stack.getItemMeta();
        sm.setDisplayName(Methods.format(getColor() + getName() + " " + getMaxLevel()));
        sm.setLore(Methods.formatList(getDescription()));
        stack.setItemMeta(sm);
        return stack;
    }
    public ItemStack toBook(Material mat, String level) {
        ItemStack stack = new ItemStack(mat);
        ItemMeta sm = stack.getItemMeta();
        sm.setDisplayName(Methods.format(getColor() + getName() + " " + level));
        sm.setLore(Methods.formatList(getDescription()));
        stack.setItemMeta(sm);
        return stack;
    }

    public ItemStack apply(ItemStack stack) {
        ItemStack item = stack.clone();
        ItemMeta meta = stack.getItemMeta();
        ArrayList<String> lore = new ArrayList<String>();
        if (meta != null) {
            meta.setLore(lore);
            lore.addAll(Methods.formatList(meta.getLore()));
            lore.add(Methods.format(getColor() + getName() + " " + getMaxLevel()));
        }
        if (meta != null) {
            meta.setLore(lore);
        }
        item.setItemMeta(meta);
        return item;
    }

    public ItemStack apply(ItemStack stack, String level) {
        ItemStack item = stack.clone();
        ItemMeta meta = stack.getItemMeta();
        ArrayList<String> lore = new ArrayList<String>();
        if (meta != null) {
            meta.setLore(lore);
            lore.addAll(Methods.formatList(meta.getLore()));
            lore.add(Methods.format(getColor() + getName() + " " + level));
        }
        if (meta != null) {
            meta.setLore(lore);
        }
        item.setItemMeta(meta);
        return item;
    }
}
