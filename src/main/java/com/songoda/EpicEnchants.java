package com.songoda;

import com.songoda.config.ConfigManager;
import com.songoda.dust.CommonDust;
import com.songoda.dust.DustRegistry;
import com.songoda.enchantments.tool.DrunkenEnchantment;
import com.songoda.enchantments.tool.MeltingEnchantment;
import com.songoda.handlers.CommandHandler;
import com.songoda.handlers.EnchantmentRegistry;
import com.songoda.handlers.ItemEnchantmentHandler;
import com.songoda.listeners.InventoryListeners;
import com.songoda.utils.Methods;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public final class EpicEnchants extends JavaPlugin {
    private static EpicEnchants INSTANCE;
    private ItemEnchantmentHandler itemEnchantmentHandler;
    private EnchantmentRegistry registry;
    public static CommandSender console = Bukkit.getConsoleSender();
    private DustRegistry dustRegistry;
    private ConfigManager configurationManager;
    @Override
    public void onEnable() {
        console.sendMessage(Methods.format("&a============================="));
        console.sendMessage(Methods.format("&7EpicEnchants " + this.getDescription().getVersion() + " by &5Songoda <3"));
        console.sendMessage(Methods.format("&7Action: &aEnabling&7..."));
        INSTANCE = this;
        this.itemEnchantmentHandler = new ItemEnchantmentHandler(this);
        this.registry = new EnchantmentRegistry();
        this.dustRegistry = new DustRegistry();
        this.configurationManager = new ConfigManager(this);
        saveResource("enchants.yml", true);
        saveResource("config.yml", true);
        new CommonDust();
        new DrunkenEnchantment();
        new MeltingEnchantment();
        console.sendMessage(Methods.format("&a============================="));
        getCommand("EpicEnchants").setExecutor(new CommandHandler());
        Bukkit.getServer().getPluginManager().registerEvents(new InventoryListeners(this), this);
    }

    @Override
    public void onDisable() {
        console.sendMessage(Methods.format("&a============================="));
        console.sendMessage(Methods.format("&7EpicEnchants " + this.getDescription().getVersion() + " by &5Songoda <3"));
        console.sendMessage(Methods.format("&7Action: &cDisabling&7..."));
        console.sendMessage(Methods.format("&a============================="));
    }

    public static EpicEnchants getInstance() {
        return INSTANCE;
    }

    public ItemEnchantmentHandler getItemEnchantmentHandler() {
        return itemEnchantmentHandler;
    }

    public EnchantmentRegistry getEnchantmentRegistry() {
        return registry;
    }

    public DustRegistry getDustRegistry() {
        return dustRegistry;
    }

    public ConfigManager getConfigurationManager() {
        return this.configurationManager;
    }
}
