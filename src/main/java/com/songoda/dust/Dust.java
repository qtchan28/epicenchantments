package com.songoda.dust;

import com.songoda.EpicEnchants;
import com.songoda.utils.Methods;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class Dust implements Listener {
    private String name;
    private double maxChance;
    private List<String> description;

    public Dust(String name, double maxChance, List<String> description) {
        this.name = name;
        this.maxChance = maxChance;
        this.description = description;
        EpicEnchants.getInstance().getDustRegistry().registerDust(name,this);
        Bukkit.getPluginManager().registerEvents(this, EpicEnchants.getInstance());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMaxChance() {
        return maxChance;
    }

    public void setMaxChance(double maxChance) {
        this.maxChance = maxChance;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }
    public ItemStack toStack(Material mat) {
        ItemStack stack = new ItemStack(mat);
        ItemMeta sm = stack.getItemMeta();
        sm.setLore(Methods.formatList(getDescription()));
        sm.setDisplayName(Methods.format(getName()));
        stack.setItemMeta(sm);
        return stack;
    }
}
