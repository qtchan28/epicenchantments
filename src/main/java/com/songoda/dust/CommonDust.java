package com.songoda.dust;

import java.util.Arrays;
import java.util.List;

public class CommonDust extends Dust {
    public CommonDust() {
        super("&7Common Dust", 30.5, Arrays.asList("&7A common enchant dust."));
    }
}
