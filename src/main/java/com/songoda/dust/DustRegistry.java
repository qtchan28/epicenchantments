package com.songoda.dust;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DustRegistry {
    private Map<String, Dust> registeredDust = new HashMap<>();

    public Dust registerDust(String name, Dust dust) {
        registeredDust.put(name.toLowerCase(), dust);
        return dust;
    }

    public Dust unregisterDust(Dust dust) {
        registeredDust.remove(dust);
        return dust;
    }

    public Dust getRegisteredDust(String name) {
        return registeredDust.get(name.toLowerCase());
    }

    public boolean isRegisteredDust(String dust) {
        return registeredDust.containsKey(dust.toLowerCase());
    }

    public Map<String, Dust> getRegisteredDust() {
        return Collections.unmodifiableMap(registeredDust);
    }

    public void clear() {
        registeredDust.clear();
    }

}
