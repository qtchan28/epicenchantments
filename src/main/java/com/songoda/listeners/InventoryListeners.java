package com.songoda.listeners;

import com.songoda.EpicEnchants;
import com.songoda.dust.Dust;
import com.songoda.enchantments.Enchantment;
import com.songoda.utils.Methods;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryListeners implements Listener {
    private EpicEnchants instance;

    public InventoryListeners(EpicEnchants instance) {
        this.instance = instance;
    }

    @EventHandler
    public void onClickInCreativeMenu(InventoryClickEvent event) {
        if (event.getInventory().getTitle().equalsIgnoreCase(Methods.format("EpicEnchants Creative Menu"))) {

            Player player = (Player) event.getWhoClicked();
            if (event.getSlot() == 0) {
                player.sendMessage(Methods.format("&aOpening enchantment book menu."));
                player.closeInventory();
                Inventory bookInventory = Bukkit.createInventory(null, 54, Methods.format("&1Enchantment Book Menu"));
                for (Enchantment enchant : EpicEnchants.getInstance().getEnchantmentRegistry().getRegisteredEnchantments().values()) {
                    bookInventory.addItem(enchant.toBook(Material.BOOK, enchant.getMaxLevel()));
                }
                player.openInventory(bookInventory);

            } else if (event.getSlot() == 1) {
                player.sendMessage(Methods.format("&aOpening &cdust menu&a."));
                player.closeInventory();
                Inventory dustInventory = Bukkit.createInventory(null, 27, Methods.format("&cDust Menu"));
                for (Dust dust : EpicEnchants.getInstance().getDustRegistry().getRegisteredDust().values()) {
                    dustInventory.addItem(dust.toStack(Material.FIREBALL));
                }
                player.openInventory(dustInventory);
            }
            event.setCancelled(true);
        }
        if (event.getInventory().getTitle().equalsIgnoreCase(Methods.format("&1Enchantment Book Menu"))) {
            Player player = (Player) event.getWhoClicked();
            player.getInventory().addItem(event.getCurrentItem());
            event.setCancelled(true);
        }

        if (event.getAction().equals(InventoryAction.SWAP_WITH_CURSOR) && event.getCursor() != null && event.getCursor().getType() != Material.AIR && event.getCursor().hasItemMeta() &&
                event.getCursor().getItemMeta().hasDisplayName() && event.getCursor().getItemMeta().hasLore()) {
            final ItemStack cursor = event.getCursor();
            final ItemStack current = event.getCurrentItem();
            for (Enchantment ench : EpicEnchants.getInstance().getEnchantmentRegistry().getRegisteredEnchantments().values()) {
                if (cursor.getItemMeta().getDisplayName().startsWith(Methods.format(ench.getColor() + ench.getName()))) {
                    Player player = (Player) event.getWhoClicked();
                    player.getInventory().addItem(ench.apply(current, ench.getMaxLevel()));
                    player.getInventory().removeItem(current);
                    if (cursor.getAmount() > 0) {
                        cursor.setAmount(cursor.getAmount() - 1);
                    }
                    if (cursor.getAmount() == 0) {
                        player.setItemOnCursor(null);
                        return;
                    }
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                }
            }
        }
    }
}
