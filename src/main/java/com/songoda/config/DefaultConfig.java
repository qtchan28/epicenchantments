package com.songoda.config;

public class DefaultConfig {
    public static final String DRUNKEN_ENCHANTMENT_PATH = "Enchantments.Drunken";
    public static final String MELTING_ENCHANTMENT_PATH = "Enchantments.Melting";
    public static final String PLUGIN_PREFIX = "&8(&7EpicEnchants&8)";
}
