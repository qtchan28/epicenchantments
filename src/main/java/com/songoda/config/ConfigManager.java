package com.songoda.config;

import com.songoda.EpicEnchants;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * @author Destructuctor on 11/25/2018
 * Hello to people reading this a century from now!
 * Is java obsolete now?
 */
public class ConfigManager {
    private EpicEnchants instance;
    private FileConfiguration enchantsConfig;
    private File enchantsFile;
    private FileConfiguration mainConfig;
    private File mainFile;

    public ConfigManager(EpicEnchants instance) {
        /**
         * Enchantments configuration file.
         */
        enchantsFile = new File(instance.getDataFolder(), "enchants.yml");
        instance.getDataFolder().mkdirs();
        if (!enchantsFile.exists()) {
            try {
                enchantsFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        enchantsConfig = YamlConfiguration.loadConfiguration(enchantsFile);
        /**
         * Main configuration file.
         */
        mainFile = new File(instance.getDataFolder(), "config.yml");
        if (!mainFile.exists()) {
            try {
                mainFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mainConfig = YamlConfiguration.loadConfiguration(mainFile);

    }

    /**
     * @return the configuration file for Enchantments.
     */
    public FileConfiguration getEnchantsConfig() {
        return enchantsConfig;
    }

    public FileConfiguration getMainConfig() {
        return mainConfig;
    }
}
